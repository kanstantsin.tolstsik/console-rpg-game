The following project represents RPG game.

At the beginning, user create Hero. He/She need to specify Hero's name, and then choose Hero's class. There are 4 available classes:
1. Elf. Class use magic for fighting.
2. Knight. Class use sward for fighting.
3. Warrior. Class use sward for fighting.
4. Wizard. Class use magic for fighting.

After Hero was created, player can explore the world. World contains generated enemies. If Hero and one or more enemies have the same location, they can fight with Hero if he/she wants.

Fight logic is very simple. If Hero uses sward, he/she wins when enemy counts is equal one only. Otherwise, Hero with sward looses.
If Hero uses magic, he/she wins when enemy counts is more than one. Otherwise, Hero with magic looses.

Therefore, game strategy is trivial. When we use magic Hero, we need to skip all fights with only one enemy. When we use sward Hero, we need to skip all fights with more than one enemies.

Player can save/resume game. You can save game if you answer No the exploration question. When you loose, the game automatically suggest you to resume saved game.

Good luck!