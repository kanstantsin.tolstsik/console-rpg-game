package com.kt.rpg.game.behaviors;

import com.kt.rpg.factory.ObjectFactory;
import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;

import java.io.Serializable;
import java.util.List;

public class SwordFightBehavior implements FightBehavior, Serializable {

    @Override
    public FightResult fight(final Character character, final List<Character> opponents) throws Exception {
        final FightResult fightResult = ObjectFactory.INSTANCE.createObject(FightResult.class);
        if (opponents.size() == 1) {
            fightResult.setWinner(character);
            fightResult.setWin(true);
        } else {
            fightResult.setWinner(opponents.get(0));
            fightResult.setWin(false);
        }
        return fightResult;
    }
}