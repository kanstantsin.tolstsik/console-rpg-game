package com.kt.rpg.game.behaviors;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;

import java.util.List;

public interface FightBehavior {

    FightResult fight (final Character character, final List<Character> opponents) throws Exception;
}
