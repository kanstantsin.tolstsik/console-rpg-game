package com.kt.rpg.game.domain;

import java.io.Serializable;

public class Experience implements Serializable {

    private int points;

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
