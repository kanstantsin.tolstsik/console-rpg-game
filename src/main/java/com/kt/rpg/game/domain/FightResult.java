package com.kt.rpg.game.domain;

import com.kt.rpg.factory.annotations.RandomInt;

import java.lang.*;

public class FightResult {

    private Character winner;

    private boolean win;

    @RandomInt(min = 1, max = 10)
    private int excellence;

    public Character getWinner() {
        return winner;
    }

    public void setWinner(Character winner) {
        this.winner = winner;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public int getExcellence() {
        return excellence;
    }

    public void setExcellence(int excellence) {
        this.excellence = excellence;
    }

    @Override
    public String toString() {
        return "FightResult{" +
                "winner=" + winner +
                ", excellence=" + excellence +
                '}';
    }
}