package com.kt.rpg.game.domain;

import com.kt.rpg.factory.annotations.Inject;

import java.io.Serializable;
import java.util.List;

public class MainHero implements Character, Serializable {

    private String name;
    private Character character;

    @Inject
    private Experience experience;

    @Override
    public FightResult fight(final List<Character> opponents) throws Exception {
        return character.fight(opponents);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}