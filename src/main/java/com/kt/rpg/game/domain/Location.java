package com.kt.rpg.game.domain;

import com.kt.rpg.factory.annotations.RandomInt;

public class Location {

    @RandomInt(min = 0, max = 10)
    private Integer coordinate;

    public Integer getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Integer coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Location location = (Location) o;

        return coordinate.equals(location.coordinate);
    }

    @Override
    public int hashCode() {
        return coordinate.hashCode();
    }
}