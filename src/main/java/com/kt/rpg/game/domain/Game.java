package com.kt.rpg.game.domain;

import com.kt.rpg.factory.annotations.GenerateList;
import com.kt.rpg.factory.annotations.Inject;
import com.kt.rpg.game.services.GameService;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class Game implements GameService, Serializable {

    @Inject
    private MainHero mainHero;

    @GenerateList(size = 10, type = Character.class)
    private List<Character> otherCharacters;

    @Inject(beanName = "gameServiceConsoleImpl")
    private GameService gameService;

    @Override
    public String createMainHeroName() {
        return gameService.createMainHeroName();
    }

    @Override
    public Character createMainHeroCharacter() {
        return gameService.createMainHeroCharacter();
    }

    @Override
    public List<Character> exploreWorld(final Game game) throws Exception {
        return gameService.exploreWorld(game);
    }

    @Override
    public FightResult fight(final List<Character> enemies, final Game game) throws Exception {
        return gameService.fight(enemies, game);
    }

    @Override
    public boolean isGameCompleted(final FightResult fightResult, final Game game) {
        return gameService.isGameCompleted(fightResult, game);
    }

    @Override
    public void gainExperience(final FightResult fightResult, final Game game) {
        gameService.gainExperience(fightResult, game);
    }

    @Override
    public void saveGame(final Game game) throws IOException {
        gameService.saveGame(game);
    }

    @Override
    public Game resumeGame() throws IOException, ClassNotFoundException {
        return gameService.resumeGame();
    }

    public MainHero getMainHero() {
        return mainHero;
    }

    public void setMainHero(MainHero mainHero) {
        this.mainHero = mainHero;
    }

    public List<Character> getOtherCharacters() {
        return otherCharacters;
    }

    public void setOtherCharacters(List<Character> otherCharacters) {
        this.otherCharacters = otherCharacters;
    }
}