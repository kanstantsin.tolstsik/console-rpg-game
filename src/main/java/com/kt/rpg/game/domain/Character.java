package com.kt.rpg.game.domain;

import com.kt.rpg.factory.ObjectFactory;

import java.util.List;

public interface Character {

    FightResult fight(final List<Character> opponents) throws Exception;

    default Location explore() throws Exception {
        return ObjectFactory.INSTANCE.createObject(Location.class);
    }
}
