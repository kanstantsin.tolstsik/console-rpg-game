package com.kt.rpg.game.domain.characters;

import com.kt.rpg.factory.annotations.Inject;
import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.behaviors.FightBehavior;
import com.kt.rpg.game.domain.FightResult;

import java.io.Serializable;
import java.util.List;

public class Elf implements Character, Serializable {

    @Inject(beanName = "magic")
    private FightBehavior fightBehavior;

    @Override
    public FightResult fight(final List<Character> opponents) throws Exception {
        return fightBehavior.fight(this, opponents);
    }

    public FightBehavior getFightBehavior() {
        return fightBehavior;
    }

    public void setFightBehavior(FightBehavior fightBehavior) {
        this.fightBehavior = fightBehavior;
    }
}
