package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.Game;
import com.kt.rpg.game.domain.Location;
import com.kt.rpg.game.domain.MainHero;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExplorationServiceImpl implements ExplorationService, Serializable {
    @Override
    public List<Character> exploreWorld(final Game game) throws Exception {
        final MainHero mainHero = game.getMainHero();
        final Location mainHeroLocation = mainHero.explore();
        final List<Character> enemies = new ArrayList<>();
        do {
            for (final Character character : game.getOtherCharacters()) {
                if (mainHeroLocation.equals(character.explore())) enemies.add(character);
            }
        } while (enemies.isEmpty());
        return enemies;
    }
}