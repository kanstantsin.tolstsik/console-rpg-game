package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.io.IOException;
import java.util.List;

public interface GameService {

    String createMainHeroName();

    Character createMainHeroCharacter();

    List<Character> exploreWorld(final Game game) throws Exception;

    FightResult fight(final List<Character> enemies, final Game game) throws Exception;

    boolean isGameCompleted(final FightResult fightResult, final Game game);

    void gainExperience(final FightResult fightResult, final Game game);

    void saveGame(final Game game) throws IOException;

    Game resumeGame() throws IOException, ClassNotFoundException;
}
