package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Game;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface MementoService {

    void saveGame(final Game game) throws IOException;

    Game resumeGame() throws IOException, ClassNotFoundException;
}
