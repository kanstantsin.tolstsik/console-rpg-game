package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

public interface ExperienceService {

    void gainExperience(final FightResult fightResult, final Game game);
}
