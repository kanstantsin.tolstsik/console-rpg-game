package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Game;

import java.io.*;

public class MementoServiceImpl implements MementoService, Serializable {

    private static final String FILE = "save.txt";

    @Override
    public void saveGame(final Game game) throws IOException {
        final FileOutputStream fos = new FileOutputStream(FILE);
        final BufferedOutputStream bos = new BufferedOutputStream(fos);
        final ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(game);
        oos.close();
    }

    @Override
    public Game resumeGame() throws IOException, ClassNotFoundException {
        final FileInputStream fis = new FileInputStream(FILE);
        BufferedInputStream bis = new BufferedInputStream(fis);
        ObjectInputStream ois = new ObjectInputStream(bis);
        Object obj = ois.readObject();
        ois.close();
        return (Game) obj;
    }
}