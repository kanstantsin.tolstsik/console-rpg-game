package com.kt.rpg.game.services;

import com.kt.rpg.factory.annotations.Inject;
import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameServiceConsoleImpl implements GameService, Serializable {

    @Inject
    private AvailableHeroesContainer container;

    @Inject
    private ExplorationService explorationService;

    @Inject
    private FightService fightService;

    @Inject
    private ExperienceService experienceService;

    @Inject
    private MementoService mementoService;

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    @Override
    public String createMainHeroName() {
        System.out.println("You need to create your Hero. Please, specify his/her name");
        final Scanner scanner = new Scanner(System.in);
        final String mainHeroName = scanner.nextLine();
        return mainHeroName;
    }

    @Override
    public Character createMainHeroCharacter() {
        Character mainHeroCharacter = null;
        do {
            System.out.println(ANSI_YELLOW + "You need to choose the class of Hero. Please, select correct number from the list:" + ANSI_RESET);
            System.out.println("1 for Elf");
            System.out.println("2 for Knight");
            System.out.println("3 for Warrior");
            System.out.println("4 for Wizard");
            final Scanner scanner = new Scanner(System.in);
            final String heroClassString = scanner.nextLine();
            switch (heroClassString) {
                case "1":
                    mainHeroCharacter = container.getElf();
                    break;
                case "2":
                    mainHeroCharacter = container.getKnight();
                    break;
                case "3":
                    mainHeroCharacter = container.getWarrior();
                    break;
                case "4":
                    mainHeroCharacter = container.getWizard();
                    break;
                default:
                    System.out.println(ANSI_RED + "Error: Incorrect input!" + ANSI_RESET);
            }
        } while (mainHeroCharacter == null);

        System.out.println(ANSI_GREEN + "Congratulations! You Hero was created!" + ANSI_RESET);
        return mainHeroCharacter;
    }

    @Override
    public List<Character> exploreWorld(final Game game) throws Exception {
        final List<Character> enemies = new ArrayList<>();
        String answer;
        do {
            System.out.println(ANSI_YELLOW + "Do you want to explore the world? (y/n)" + ANSI_RESET);
            final Scanner scanner = new Scanner(System.in);
            answer = scanner.nextLine().toLowerCase();
            if ("y".equals(answer)) {
                enemies.addAll(explorationService.exploreWorld(game));
                System.out.println(ANSI_RED + "You have found " + enemies.size() + " enemy(ies)!" + ANSI_RESET);
            }
            if (!isCorrectAnswer(answer)) {
                System.out.println(ANSI_RED + "Error: Incorrect input!" + ANSI_RESET);
            }
        } while (!isCorrectAnswer(answer));

        return enemies;
    }

    @Override
    public FightResult fight(final List<Character> enemies, final Game game) throws Exception {
        FightResult fightResult = null;
        String answer;
        do {
            System.out.println(ANSI_RED + "Do you want to fight? (y/n)" + ANSI_RESET);
            final Scanner scanner = new Scanner(System.in);
            answer = scanner.nextLine().toLowerCase();
            if ("y".equals(answer)) {
                fightResult = fightService.fight(enemies, game);
                if (fightResult.isWin()) {
                    System.out.println(ANSI_GREEN + "You won! Congratulations!" + ANSI_RESET);
                }
            } else if ("n".equals(answer)) {
                fightResult = new FightResult();
                System.out.println(ANSI_YELLOW + "You are poltroon!" + ANSI_RESET);
            }
            if (!isCorrectAnswer(answer)) {
                System.out.println(ANSI_RED + "Error: Incorrect input!" + ANSI_RESET);
            }
        } while (!isCorrectAnswer(answer));

        return fightResult;
    }

    @Override
    public boolean isGameCompleted(final FightResult fightResult, final Game game) {
        final boolean isLooseGame = !fightResult.isWin() && fightResult.getWinner() != null;
        final boolean isWinGame = game.getOtherCharacters().isEmpty();
        if (isLooseGame) System.out.println(ANSI_RED + "You loose... Game over..." + ANSI_RESET);
        if (isWinGame)
            System.out.println(ANSI_BLUE + "You won! You got " + game.getMainHero().getExperience().getPoints() + " points!" + ANSI_RESET);
        return isLooseGame || isWinGame;
    }

    @Override
    public void gainExperience(final FightResult fightResult, final Game game) {
        experienceService.gainExperience(fightResult, game);
    }

    @Override
    public void saveGame(final Game game) throws IOException {
        String answer;
        do {
            System.out.println(ANSI_YELLOW + "Do you want to save the game? (y/n)" + ANSI_RESET);
            final Scanner scanner = new Scanner(System.in);
            answer = scanner.nextLine().toLowerCase();
            if ("y".equals(answer)) {
                mementoService.saveGame(game);
                System.out.println(ANSI_GREEN + "Game was saved successfully." + ANSI_RESET);
            }
            if (!isCorrectAnswer(answer)) {
                System.out.println(ANSI_RED + "Error: Incorrect input!" + ANSI_RESET);
            }
        } while(!isCorrectAnswer(answer));
    }

    @Override
    public Game resumeGame() {
        Game game = null;
        String answer;
        do {
            System.out.println(ANSI_YELLOW + "Do you want to resume the game? If no, new game will start. (y/n)" + ANSI_RESET);
            final Scanner scanner = new Scanner(System.in);
            answer = scanner.nextLine().toLowerCase();
            if ("y".equals(answer)) {
                try {
                    game = mementoService.resumeGame();
                } catch (final Exception e) {
                    return game;
                }
            }
            if (!isCorrectAnswer(answer)) {
                System.out.println(ANSI_RED + "Error: Incorrect input!" + ANSI_RESET);
            }
        } while(!isCorrectAnswer(answer));

        return game;
    }

    private boolean isCorrectAnswer(final String answer) {
        return answer.equals("y") || answer.equals("n");
    }
}