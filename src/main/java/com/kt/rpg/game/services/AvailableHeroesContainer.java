package com.kt.rpg.game.services;

import com.kt.rpg.factory.annotations.Inject;
import com.kt.rpg.game.domain.Character;

import java.io.Serializable;

public class AvailableHeroesContainer implements Serializable {

    @Inject(beanName = "elf")
    private Character elf;

    @Inject(beanName = "knight")
    private Character knight;

    @Inject(beanName = "warrior")
    private Character warrior;

    @Inject(beanName = "wizard")
    private Character wizard;

    public Character getElf() {
        return elf;
    }

    public void setElf(Character elf) {
        this.elf = elf;
    }

    public Character getKnight() {
        return knight;
    }

    public void setKnight(Character knight) {
        this.knight = knight;
    }

    public Character getWarrior() {
        return warrior;
    }

    public void setWarrior(Character warrior) {
        this.warrior = warrior;
    }

    public Character getWizard() {
        return wizard;
    }

    public void setWizard(Character wizard) {
        this.wizard = wizard;
    }
}