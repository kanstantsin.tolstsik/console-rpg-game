package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.io.Serializable;

public class ExperienceServiceImpl implements ExperienceService, Serializable {
    @Override
    public void gainExperience(final FightResult fightResult, final Game game) {
        final int experience = fightResult.getExcellence();
        int heroExperience = game.getMainHero().getExperience().getPoints();
        heroExperience += experience;
        game.getMainHero().getExperience().setPoints(heroExperience);
    }
}