package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.io.Serializable;
import java.util.List;

public class FightServiceImpl implements FightService, Serializable {
    @Override
    public FightResult fight(final List<Character> enemies, final Game game) throws Exception {
        final Character mainHeroCharacter = game.getMainHero().getCharacter();
        final FightResult fightResult = mainHeroCharacter.fight(enemies);
        if (fightResult.isWin()) {
            game.getOtherCharacters().removeAll(enemies);
        }
        return fightResult;
    }
}