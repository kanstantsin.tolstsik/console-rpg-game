package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.Game;

import java.util.List;

public interface ExplorationService {

    List<Character> exploreWorld(final Game game) throws Exception;
}
