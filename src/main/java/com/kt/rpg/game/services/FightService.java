package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.util.List;

public interface FightService {

    FightResult fight(final List<Character> enemies, final Game game) throws Exception;
}
