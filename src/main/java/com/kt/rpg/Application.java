package com.kt.rpg;

import com.kt.rpg.factory.ObjectFactory;
import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;

import java.util.List;

public class Application {

    public static void main(final String[] args) throws Exception {

        do {
            //Start new game
            Game game = ObjectFactory.INSTANCE.createObject(Game.class);

            //Create Hero
            final String mainHeroName = game.createMainHeroName();
            final Character mainHeroCharacter = game.createMainHeroCharacter();
            game.getMainHero().setName(mainHeroName);
            game.getMainHero().setCharacter(mainHeroCharacter);

            //Explore and fight while win
            do {
                final List<Character> enemies = game.exploreWorld(game);
                FightResult fightResult;
                if (!enemies.isEmpty()) {
                    fightResult = game.fight(enemies, game);
                    if (fightResult.isWin())
                        game.gainExperience(fightResult, game);
                    if (game.isGameCompleted(fightResult, game)) {
                        final Game tempGame = game.resumeGame();
                        if (tempGame != null) {
                            game = tempGame;
                        } else break;
                    }
                } else {
                    game.saveGame(game);
                }
            } while (true);
        } while (true);
    }
}