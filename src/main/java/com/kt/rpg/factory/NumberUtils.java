package com.kt.rpg.factory;

import java.util.Random;

public class NumberUtils {

    public static int getRandomNumberInRange(final int min, final int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max should be greater than min");
        }
        final Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
