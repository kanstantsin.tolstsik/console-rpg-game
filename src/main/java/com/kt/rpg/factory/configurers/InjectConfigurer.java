package com.kt.rpg.factory.configurers;

import com.kt.rpg.factory.Configuration;
import com.kt.rpg.factory.ConfigurationImpl;
import com.kt.rpg.factory.ObjectFactory;
import com.kt.rpg.factory.annotations.Inject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Optional;

public class InjectConfigurer implements ObjectConfigurer {

    private Configuration configuration = new ConfigurationImpl();

    public InjectConfigurer() throws IOException {
    }

    @Override
    public <T> void configure(final T object) throws Exception {
        final Class<?> clazz = object.getClass();
        final Field[] fields = clazz.getDeclaredFields();
        for (final Field field : fields) {
            if (field.isAnnotationPresent(Inject.class)) {
                String beanName = field.getAnnotation(Inject.class).beanName();
                beanName = Optional.ofNullable(beanName).orElse("");
                final Class classToInject = (beanName.isEmpty()) ?
                        field.getType() : configuration.getImplementation(beanName);
                final Object object2Inject = ObjectFactory.INSTANCE.createObject(classToInject);
                field.setAccessible(true);
                field.set(object, object2Inject);
            }
        }
    }
}