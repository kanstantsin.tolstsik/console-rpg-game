package com.kt.rpg.factory.configurers;

public interface ObjectConfigurer {

    <T> void configure(final T object) throws Exception;
}
