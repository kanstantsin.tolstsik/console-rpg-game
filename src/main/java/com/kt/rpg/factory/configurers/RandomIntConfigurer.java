package com.kt.rpg.factory.configurers;

import com.kt.rpg.factory.NumberUtils;
import com.kt.rpg.factory.annotations.RandomInt;

import java.lang.reflect.Field;

public class RandomIntConfigurer implements ObjectConfigurer {

    @Override
    public <T> void configure(final T object) throws Exception {
        final Class<?> clazz = object.getClass();
        final Field[] fields = clazz.getDeclaredFields();
        for (final Field field : fields) {
            if (field.isAnnotationPresent(RandomInt.class)) {
                final int min = field.getAnnotation(RandomInt.class).min();
                final int max = field.getAnnotation(RandomInt.class).max();
                if (min >= max) {
                    throw new RuntimeException("Please specify correct min/max values for " +
                            RandomInt.class);
                }
                final int value = NumberUtils.getRandomNumberInRange(min, max);
                field.setAccessible(true);
                field.set(object, value);
            }
        }
    }
}