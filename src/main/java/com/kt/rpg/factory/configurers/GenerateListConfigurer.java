package com.kt.rpg.factory.configurers;

import com.kt.rpg.factory.NumberUtils;
import com.kt.rpg.factory.ObjectFactory;
import com.kt.rpg.factory.ReflectionUtils;
import com.kt.rpg.factory.annotations.GenerateList;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateListConfigurer implements ObjectConfigurer {

    private static final String CHARACTER_PACKAGE = "com.kt.rpg.game.domain.characters";

    @Override
    public <T> void configure(final T object) throws Exception {
        final Class<?> clazz = object.getClass();
        final Field[] fields = clazz.getDeclaredFields();
        for (final Field field : fields) {
            if (field.isAnnotationPresent(GenerateList.class)) {
                final int size = field.getAnnotation(GenerateList.class).size();
                final Class type = field.getAnnotation(GenerateList.class).type();
                if (size == 0 || type == null) {
                    throw new RuntimeException("Please specify correct size and type parameters for " +
                            GenerateList.class);
                }
                final List<Object> list = new ArrayList<>(size);
                final List<Class> classes2Inject = new ArrayList<>(ReflectionUtils.getSubTypesOf(type, CHARACTER_PACKAGE));
                for (int i = size; i > 0; i--) {
                    final int index = NumberUtils.getRandomNumberInRange(0, classes2Inject.size() - 1);
                    final Class classToInject = classes2Inject.get(index);
                    final Object object2Inject = ObjectFactory.INSTANCE.createObject(classToInject);
                    list.add(object2Inject);
                }
                field.setAccessible(true);
                field.set(object, list);
            }
        }
    }
}