package com.kt.rpg.factory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;

public class ClassPathScanner {

    private String packageToScan;

    public ClassPathScanner(final String packageToScan) {
        this.packageToScan = packageToScan;
    }

    public <T> Class<T> scanPackages(Class<T> type) throws URISyntaxException, IOException, ClassNotFoundException {
        Class<T> resultClass;

        final Set<Class> classes = ReflectionUtils.getSubTypesOf(type, packageToScan);
        if (classes.size() == 0) {
            throw new RuntimeException("No implementation found for the interface " + type);
        }
        if (classes.size() > 1) {
            throw new RuntimeException("Expected implementation wasn't found for the interface " + type +
                    ". Please, add appropriate mapping into configuration.");
        }

        resultClass = classes.iterator().next();
        return resultClass;
    }
}
