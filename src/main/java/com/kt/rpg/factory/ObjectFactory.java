package com.kt.rpg.factory;

import com.kt.rpg.factory.annotations.PostConstruct;
import com.kt.rpg.factory.configurers.ObjectConfigurer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public enum ObjectFactory {

    INSTANCE;

    public static final String BASE_PACKAGE = "com.kt.rpg";

    private ClassPathScanner scanner = new ClassPathScanner(BASE_PACKAGE);
    private List<ObjectConfigurer> configurers = new ArrayList<>();

    ObjectFactory() {
        final Set<Class> classes;
        try {
            classes = ReflectionUtils.getSubTypesOf(ObjectConfigurer.class, BASE_PACKAGE);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
        for (final Class clazz : classes) {
            try {
                configurers.add((ObjectConfigurer) clazz.newInstance());
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public <T> T createObject(final Class<T> type) throws Exception {
        Class<T> resultClass;
        resultClass = findImplementation(type);
        final T object = resultClass.newInstance();
        configureObject(object);
        invokePostConstructMethods(object);
        return object;
    }

    private <T> void invokePostConstructMethods(final T object) throws Exception {
        final Method[] methods = object.getClass().getMethods();
        for (final Method method : methods) {
            if (method.isAnnotationPresent(PostConstruct.class)) {
                method.invoke(object);
            }
        }
    }

    private <T> Class<T> findImplementation(final Class<T> type) throws Exception {
        Class<T> resultClass;
        if (type.isInterface()) {
            resultClass = scanner.scanPackages(type);
        } else {
            resultClass = type;
        }
        return resultClass;
    }

    private <T> void configureObject(final T object) throws Exception {
        for (final ObjectConfigurer configurer : configurers) {
            configurer.configure(object);
        }
    }
}