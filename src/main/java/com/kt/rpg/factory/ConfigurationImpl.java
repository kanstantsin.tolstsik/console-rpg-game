package com.kt.rpg.factory;

import java.io.*;
import java.util.*;

public class ConfigurationImpl implements Configuration {

    private static final String PROPERTY_FILE = "application.properties";
    final Properties properties = new Properties();

    public ConfigurationImpl() throws IOException {
        final ClassLoader classLoader = getClass().getClassLoader();
        final InputStream inputStream = classLoader.getResourceAsStream(PROPERTY_FILE);
        if (inputStream == null) throw new RuntimeException(PROPERTY_FILE + " is not found");
        properties.load(inputStream);
    }

    @Override
    public Class getImplementation(final String beanName) {
        final String className = (String) properties.get(beanName);
        Class clazz;
        try {
            clazz = Class.forName(className);
        } catch (final ClassNotFoundException e) {
            throw new RuntimeException("Can not find implementation for bean name" + beanName);
        }
        return clazz;
    }
}