package com.kt.rpg.factory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class ReflectionUtils {

    private static Set<Class> getClasses(final String packageName) throws ClassNotFoundException, IOException, URISyntaxException {
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final String path = packageName.replace('.', '/');
        final Enumeration<URL> resources = classLoader.getResources(path);
        final List<File> directories = new ArrayList<>();

        final Set<Class> classes = new HashSet<>();

        while (resources.hasMoreElements()) {
            final URL resource = resources.nextElement();

            if (isJarResource(resource)) {
                return findClassesInJar(resource);
            }

            final URI uri = new URI(resource.toString());
            directories.add(new File(uri.getPath()));
        }

        for (final File directory : directories) {
            classes.addAll(findClasses(directory, packageName));
        }

        return classes;
    }

    private static Set<Class> findClassesInJar(final URL resource) throws IOException, ClassNotFoundException {

        final Set<Class> classes = new HashSet<>();

        final String pathToJar = resource.getPath().split("!", 2)[0].replace("file:/", "");
        final JarFile jarFile = new JarFile(pathToJar);
        final Enumeration<JarEntry> enumeration = jarFile.entries();

        final URL[] urls = {new URL("jar:" + resource.getPath())};
        final URLClassLoader urlClassLoader = URLClassLoader.newInstance(urls);

        while (enumeration.hasMoreElements()) {
            final JarEntry jarEntry = enumeration.nextElement();
            if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")) {
                continue;
            }

            final String className = jarEntry.getName().substring(0, jarEntry.getName().length() - 6).replace('/', '.');
            final Class clazz = urlClassLoader.loadClass(className);
            classes.add(clazz);
        }
        return classes;
    }

    public static Set<Class> getSubTypesOf(final Class type, final String packageName) throws URISyntaxException, IOException, ClassNotFoundException {
        final Set<Class> resultClasses = getClasses(packageName).stream()
                .filter(item -> type.isAssignableFrom(item) && !item.isInterface())
                .collect(Collectors.toSet());
        return resultClasses;
    }

    private static List<Class> findClasses(final File directory, final String packageName) throws ClassNotFoundException {
        final List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        final File[] files = directory.listFiles();
        for (final File file : files) {
            if (file.isDirectory()) {
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    private static boolean isJarResource(final URL resource) {
        return resource.getPath().contains("jar!");
    }
}