package com.kt.rpg.factory;

public interface Configuration {

    Class getImplementation(final String beanName);
}