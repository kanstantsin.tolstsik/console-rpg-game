package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Experience;
import com.kt.rpg.game.domain.Game;
import com.kt.rpg.game.domain.MainHero;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MementoServiceImplTest {

    private static final int EXP_POINTS = 5;

    private MementoService mementoService;
    private Game game;
    private MainHero mainHero;
    private Experience experience;

    @Before
    public void setUp() throws Exception {
        mementoService = new MementoServiceImpl();
        game = new Game();
        mainHero = new MainHero();
        experience = new Experience();
        experience.setPoints(EXP_POINTS);
        mainHero.setExperience(experience);
        game.setMainHero(mainHero);
    }

    @Test
    public void saveAndResumeGame() throws Exception {
        mementoService.saveGame(game);
        final Game resumedGame = mementoService.resumeGame();
        assertEquals(EXP_POINTS, resumedGame.getMainHero().getExperience().getPoints());
    }
}