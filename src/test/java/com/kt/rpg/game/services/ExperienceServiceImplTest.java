package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExperienceServiceImplTest {

    private static final int HERO_EXPERIENCE = 5;
    private static final int FIGHT_EXCELENCE = 2;

    private ExperienceService experienceService;
    private FightResult fightResult;
    private Game game;
    private MainHero mainHero;
    private Experience experience;

    @Before
    public void setUp() throws Exception {
        experienceService = new ExperienceServiceImpl();
        fightResult = new FightResult();
        fightResult.setExcellence(FIGHT_EXCELENCE);
        game = new Game();
        mainHero = new MainHero();
        experience = new Experience();
        experience.setPoints(HERO_EXPERIENCE);
        mainHero.setExperience(experience);
        game.setMainHero(mainHero);
    }

    @Test
    public void gainExperience() throws Exception {
        experienceService.gainExperience(fightResult, game);
        assertEquals(game.getMainHero().getExperience().getPoints(), HERO_EXPERIENCE + FIGHT_EXCELENCE);
    }

}