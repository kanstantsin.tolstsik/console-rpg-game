package com.kt.rpg.game.services;

import com.kt.rpg.game.behaviors.MagicFightBehavior;
import com.kt.rpg.game.behaviors.SwordFightBehavior;
import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.FightResult;
import com.kt.rpg.game.domain.Game;
import com.kt.rpg.game.domain.MainHero;
import com.kt.rpg.game.domain.characters.Elf;
import com.kt.rpg.game.domain.characters.Knight;
import com.kt.rpg.game.domain.characters.Wizard;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FightServiceImplTest {

    private FightService fightService;
    private List<Character> enemies;
    private Game game;
    private MainHero mainHero;
    private Character mainHeroCharacter;

    @Before
    public void setUp() throws Exception {
        fightService = new FightServiceImpl();
        game = new Game();
        mainHero = new MainHero();
        enemies = new ArrayList<>();
        game.setMainHero(mainHero);
        game.setOtherCharacters(enemies);
    }

    @Test
    public void fightWinForSward() throws Exception {
        final Elf elf = new Elf();
        elf.setFightBehavior(new MagicFightBehavior());
        enemies.add(elf);
        final Knight knight = new Knight();
        knight.setFightBehavior(new SwordFightBehavior());
        mainHero.setCharacter(knight);
        final FightResult fightResult = fightService.fight(enemies, game);
        assertTrue(fightResult.isWin());
    }

    @Test
    public void fightLooseForSward() throws Exception {
        final Elf elf = new Elf();
        elf.setFightBehavior(new MagicFightBehavior());
        final Knight knight = new Knight();
        knight.setFightBehavior(new SwordFightBehavior());
        enemies.add(elf);
        enemies.add(knight);
        mainHero.setCharacter(knight);
        final FightResult fightResult = fightService.fight(enemies, game);
        assertFalse(fightResult.isWin());
    }

    @Test
    public void fightWinForWizard() throws Exception {
        final Wizard wizard = new Wizard();
        wizard.setFightBehavior(new MagicFightBehavior());
        final Knight knight = new Knight();
        knight.setFightBehavior(new SwordFightBehavior());
        enemies.add(wizard);
        enemies.add(knight);
        mainHero.setCharacter(wizard);
        final FightResult fightResult = fightService.fight(enemies, game);
        assertTrue(fightResult.isWin());
    }

    @Test
    public void fightLooseForWizard() throws Exception {
        final Wizard wizard = new Wizard();
        wizard.setFightBehavior(new MagicFightBehavior());
        enemies.add(wizard);
        mainHero.setCharacter(wizard);
        final FightResult fightResult = fightService.fight(enemies, game);
        assertFalse(fightResult.isWin());
    }
}