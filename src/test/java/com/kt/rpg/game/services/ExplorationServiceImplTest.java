package com.kt.rpg.game.services;

import com.kt.rpg.game.domain.Character;
import com.kt.rpg.game.domain.Game;
import com.kt.rpg.game.domain.MainHero;
import com.kt.rpg.game.domain.characters.Elf;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExplorationServiceImplTest {

    private ExplorationService explorationService;
    private Game game;
    private MainHero mainHero;
    private List<Character> characters;

    @Before
    public void setUp() throws Exception {
        explorationService = new ExplorationServiceImpl();
        game = new Game();
        mainHero = new MainHero();
        characters = new ArrayList<>();
        characters.add(new Elf());
        game.setMainHero(mainHero);
        game.setOtherCharacters(characters);
    }

    @Test
    public void exploreWorld() throws Exception {
        final List<Character> enemies = explorationService.exploreWorld(game);
        assertEquals(characters.size(), enemies.size());
    }
}